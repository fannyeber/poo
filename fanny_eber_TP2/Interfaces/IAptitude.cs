﻿

using fanny_eber_TP1.Class.SpaceShip;
using System.Collections.Generic;

namespace fanny_eber_TP1.Interfaces
{
    interface IAptitude
    {
        void Use(List<SpaceShip> spaceShips);
    }
}
