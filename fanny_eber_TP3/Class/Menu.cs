﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class
{
    public class Menu
    {
        SpaceInvaders game;
        public Menu()
        {
            game = new SpaceInvaders();
            NavigationMenu();
        }

        public void NavigationMenu()
        {
            int choice = -1;
            while (choice < 1 || choice > 4)
            {
                Console.WriteLine("------------MENU------------\n1: Gestion des joueurs\n2: Gestion des vaisseaux\n3: Gestion de l'armurerie\n4: Jouer");
                choice = int.Parse(Console.ReadLine());
            }
            switch (choice)
            {
                case 1:
                    PlayerMenu();
                    break;
                case 2:
                    SpaceShipMenu();
                    break;
                case 3:
                    ArmoryMenu();
                    break;
                case 4:
                    while (!game.Player.Ship.GetIsDestroy() && game.Enemy.Where(e => !e.GetIsDestroy()).ToList().Count != 0)
                    {
                        game.Round();
                    }
                    if (game.PlayerHasWon())
                        Console.WriteLine("LE JOUEUR A GAGNE !!!!!!!!!!!!!!!! YOUHOUUUUUUUUUUUUUUUUUUU");
                    else
                        Console.WriteLine("le joueur à perdu ..... bouhouhou c'est triste mais c'est la vie");
                    game = new SpaceInvaders();
                    break;

            }
            NavigationMenu();
        }

        public void PlayerMenu()
        {
            int choice = -1;
            while (choice < 1 || choice > 4)
            {
                Console.WriteLine("------------Gestion de joueurs------------\n1: Création d'un joueur\n2: Suppression d'un joueur\n3: Choix d'un joueur\n4: Retour au menu");
                choice = int.Parse(Console.ReadLine());
            }
            switch (choice)
            {
                case 1:
                    CreatePlayer();
                    break;
                case 2:
                    DeletePlayer();
                    break;
                case 3:
                    SelectPlayer();
                    break;
                case 4:
                    NavigationMenu();
                    break;

            }
        }

        public void CreatePlayer()
        {
            Console.WriteLine("Entrez votre prénom :");
            string firstname = Console.ReadLine();

            Console.WriteLine("Entrez votre nom :");
            string lastName = Console.ReadLine();

            Console.WriteLine("Entrez votre pseudo :");
            string pseudo = Console.ReadLine();
            Player p = new(firstname, lastName, pseudo);
            game.
            Players.Add(p);
            PlayerMenu();
        }

        public void DeletePlayer()
        {
            DisplayPlayers();
            Console.WriteLine("Quel est le pseudo du joueur que voulez vous supprimer ?");
            string pseudo = Console.ReadLine();
            var p = game.Players.FirstOrDefault(x => x.Pseudo == pseudo);
            if (p != null)
            {
                game.Players.Remove(p);
                if (game.Player == p)
                {
                    game.Player = game.Players.FirstOrDefault();
                    Console.WriteLine("Vous venez de supprimer le joueur selectionné");
                    SelectPlayer();

                }
                else
                {
                    Console.WriteLine("Le joueur est bien supprimé \n");
                    PlayerMenu();
                }
            }

            PlayerMenu();

        }

        public void SelectPlayer()
        {
            DisplayPlayers();
            Console.WriteLine("Quel est le pseudo du joueur que vous voulez selectionné ?");
            string pseudo = Console.ReadLine();
            var p = game.Players.FirstOrDefault(x => x.Pseudo == pseudo);
            if (p != null)
            {
                game.Player = p;
                Console.WriteLine("Le joueur est selectionné \n");
                PlayerMenu();
            }
        }

        public void DisplayPlayers()
        {
            foreach (var p in game.Players)
            {
                Console.WriteLine(p.ToString() + "\n");
            }
        }

        public void SpaceShipMenu()
        {
            int choice = -1;
            while (choice < 1 || choice > 4)
            {
                Console.WriteLine("------------Gestion du vaisseau------------\n1: AJouter une arme\n2: Suppression une arme\n3: Retour au menu");
                choice = int.Parse(Console.ReadLine());
            }
            switch (choice)
            {
                case 1:
                    AddWeapon();
                    break;
                case 2:
                    DeleteWeapon();
                    break;
                case 3:
                    NavigationMenu();
                    break;

            }
        }

        public void AddWeapon()
        {
            DisplayWeaponArmory();
            if (game.Player.Ship.Weapons.Count >= 3)
            {
                Console.WriteLine("Votre vaisseau à déjà son nombre maximum d'arme");
                SpaceShipMenu();
            }
            else
            {
                Console.WriteLine("Quel est le nom de l'arme que tu veux ajouter a ton vaisseau ? ");
                string nameWeapon = Console.ReadLine();
                var w = Armory.Instance.Weapons.Select(x => x.Name == nameWeapon);
                if (w.Any())
                {
                    game.Player.Ship.AddWeapon(Armory.Instance.Weapons.Where(x => x.Name == nameWeapon).FirstOrDefault());
                    Console.WriteLine("L'arme à été ajouté à votre vaisseau");
                }
                SpaceShipMenu();
            }
        }

        public void DeleteWeapon()
        {
            DisplayWeaponSpaceShip();
            if (game.Player.Ship.Weapons.Count <= 0)
            {
                Console.WriteLine("Votre vaisseau n'a pas d'arme");
                SpaceShipMenu();
            }
            else
            {
                Console.WriteLine("Quel est le nom de l'arme que tu veux supprimer de ton vaisseau ? ");
                string nameWeapon = Console.ReadLine();
                var w = game.Player.Ship.Weapons.Select(x => x.Name == nameWeapon);
                if (w.Any())
                {
                    game.Player.Ship.RemoveWeapon(game.Player.Ship.Weapons.Where(x => x.Name == nameWeapon).FirstOrDefault());
                    Console.WriteLine("L'arme à été supprimer du vaisseau");
                }
                SpaceShipMenu();
            }
        }

        public void DisplayWeaponSpaceShip()
        {
            Console.WriteLine("Les armes présentes dans le vaisseau : \n");
            foreach (var w in game.Player.Ship.Weapons)
            {
                Console.WriteLine(w.ToString());
            }
        }

        public void DisplayWeaponArmory()
        {
            Console.WriteLine("Les armes présentes dans l'armurerie : \n");
            foreach (var w in Armory.Instance.Weapons)
            {
                Console.WriteLine(w.ToString());
            }
        }


        public void ArmoryMenu()
        {
            int choice = -1;
            while (choice < 1 || choice > 3)
            {
                Console.WriteLine("------------Gestion de l'armurerie------------\n1: Ajouter une arme\n2: Suppression d'une arme\n3: Retour au menu");
                choice = int.Parse(Console.ReadLine());
            }
            switch (choice)
            {
                case 1:
                    AddWeaponArmory();
                    break;
                case 2:
                    DeleteWeaponArmory();
                    break;
                case 3:
                    NavigationMenu();
                    break;

            }


        }

        public void AddWeaponArmory()
        {
            Console.WriteLine("Quel sera le nom de l'arme ? ");
            var name = Console.ReadLine();
            var minDamage = 0;
            var maxDamage = 0;
            while (minDamage >= maxDamage)
            {
                Console.WriteLine("Quel sera son nombre minimum de dégat ?");
                minDamage = int.Parse(Console.ReadLine());
                Console.WriteLine("Quel sera son nombre de dégats maximum ?");
                maxDamage = int.Parse(Console.ReadLine());
            }

            var reloadDuration = 0;
            while (reloadDuration <= 0)
            {
                Console.WriteLine("EN combien de temps ton arme se recharge ?");
                reloadDuration = int.Parse(Console.ReadLine());
            }

            var type = 0;
            while (type > 3 || type < 1)
            {

                Console.WriteLine("Quel est le type d'arme ? \n1) direct,\n2) explosive,\n3) guidé ");
                type = int.Parse(Console.ReadLine());
            }
            Armory.Instance.Weapons.Add(new Weapon((Enum.Type)type, name, minDamage, maxDamage, reloadDuration));

            ArmoryMenu();
        }

        public void DeleteWeaponArmory()
        {
            DisplayWeaponArmory();
            Console.WriteLine("Quel est le nom de l'arme que tu veux supprimer de l'armurerie ?");
            string name = Console.ReadLine();
            var w = Armory.Instance.Weapons.Where(x => x.Name == name).FirstOrDefault();
            Armory.Instance.Weapons.Remove(w);
            ArmoryMenu();
        }

    }
}
