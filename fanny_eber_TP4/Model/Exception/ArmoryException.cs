﻿using System;

namespace fanny_eber_TP1.Class
{
    public class ArmoryException : Exception
    {
        //Constructeurs pour lever l'exception ArmoryException
        public ArmoryException(): base(){

        }

        public ArmoryException(string message) : base(message)
        {

        }

        public ArmoryException(string message, Exception innerException) : base(message, innerException)
        {

        }

    }
}