﻿using System;
using System.Collections.Generic;
using System.Linq;
using fanny_eber_TP1.Class;
using fanny_eber_TP1.Class.SpaceShip;
using fanny_eber_TP1.Interfaces;

namespace fanny_eber_TP1
{
    public class SpaceInvaders
    {
        public Player Player { get; set; }
        public List<Player> Players { get; set; }
        public List<SpaceShip> Enemy { get; set; }
        private static List<SpaceShip> Ships { get; set; }
        private Armory Armory { get; }

        public SpaceInvaders()
        {
            Players = new List<Player>();
            Player = new Player("Fanny", "Eber", "Pandabrutie");
            Players.Add(Player);
            Armory = Armory.Instance;
            Enemy = Init();
            Ships = new List<SpaceShip>();
            Ships.Add(Player.Ship);
            Ships.AddRange(Enemy);
        }

        private List<SpaceShip> Init()
        {
            List<SpaceShip> ennemy = new List<SpaceShip>();
            ennemy.Add(new B_Wings());
            ennemy.Add(new Dart());
            ennemy.Add(new F_18());
            ennemy.Add(new Rocinante());
            ennemy.Add(new Tardis());
            return ennemy;
        }

        public void Round()
        {
            int count = 0;
            bool alreadyAttack = false;
            var rnd = new Random();

            Healed();

            var listWithoutModification = new List<SpaceShip>();
            listWithoutModification.AddRange(Ships);
            //si les vaisseaux ont des aptitudes
            foreach(var e in listWithoutModification)
            {
                if(e is IAptitude newShip)
                {
                    newShip.Use(Ships);
                }
            }

            // Elle devra faire jouer chaque vaisseau l’un après l’autre dans l’ordre de la liste d'ennemis.
            foreach (var e in Enemy)
            {
                //Il aura[1 / nombre d’ennemis en vie] chances de tirer en premier
                //[2 / nombre d’ennemis en vie] chances de tirer en deuxième et ainsi de suite.
                var alive = Enemy.Where(x=>!x.GetIsDestroy()).ToList();
                if (rnd.Next(0, alive.Count-1) <=count && !alreadyAttack && !Player.Ship.GetIsDestroy())
                {
                    //Le joueur tire sur un vaisseau non détruit au hasard dans la liste d’ennemis.
                    Player.Ship.Attack(alive[rnd.Next(0,alive.Count-1)]);
                    alreadyAttack = true;
                }
                //Tous les ennemis tirent sur le vaisseau du joueur.
                e.Attack(Player.Ship);
                count++;
            }

        }

        private void Healed()
        {
            //Chaque début de tour les vaisseaux ayant perdu des points de bouclier en regagne maximum 2.
            var damaged = Ships.Where(e => e.ShieldPoints != e.MaxShieldPoints && !e.GetIsDestroy()).ToList();
            
            foreach(var e in damaged)
            {
                if (e.ShieldPoints + 2 > e.MaxShieldPoints)
                    e.ShieldPoints = e.MaxShieldPoints;
                else
                    e.ShieldPoints += 2;
            }

        }

        public bool PlayerHasWon()
        {
            return Player.Ship.GetIsDestroy() ? false : true;
        }

        
        static void Main(string path)
        {
            var importWeapon = new WeaponImportor();
            //importWeapon.getFrequencyWords(path);
            importWeapon.generateWeapon(path);
            var biggestDamageWeapons=Armory.Instance.Get5BiggestAverageDamageWeapons();
            var biggestMinDamageWeapon = Armory.Instance.Get5BiggestMinDamageWeapons();

            Console.WriteLine("Les armes avec les plus gros dommages moyens : ");
            foreach(var item in biggestDamageWeapons)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine("Les armes avec les dommages minimums les plus hauts");
            foreach(var item in biggestMinDamageWeapon)
            {
                Console.WriteLine(item.ToString());
            }

            Menu m = new Menu();
        }

        //Méthode appelé en premier a l'execution
        //static void Main()
        //{
        //    Menu m = new Menu();
        //}


    }
}
