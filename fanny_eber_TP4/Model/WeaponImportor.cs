﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace fanny_eber_TP1.Class
{
    public class WeaponImportor
    {
        public Dictionary<string, int> frequencyWords;
        public int minSizeWord = 0;
        public List<string> blackList;

        public WeaponImportor() {
            frequencyWords = new Dictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);
            blackList = new List<string>();
        }

        public WeaponImportor(int _minSizeWord,List<string> _blackList)
        {
            frequencyWords = new Dictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);
            minSizeWord = _minSizeWord;
            blackList = _blackList;
        }


        public void getFrequencyWords(string path)
        {
            Regex regex = new Regex("[.?!,;:]*(?=[.?!,;:]$)");
            string input = regex.Replace(File.ReadAllText(path).ToLower(), "");
            var arr = input.Split(' ');

            foreach (var item in arr)
            {
                if (validateWord(item))
                {
                    if (frequencyWords.ContainsKey(item))
                        frequencyWords[item]++;
                    else
                        frequencyWords.Add(item, 1);
                }
            }
        }

        public bool validateWord(string word)
        {
            if (word.Length < minSizeWord)
                return false;
            if (blackList.Contains(word))
                return false;
            return true;
        }

        public void generateWeapon(string path)
        {
            getFrequencyWords(path);
            List<Weapon> w = new List<Weapon>();
            Random rnd = new Random();

            foreach(var item in frequencyWords)
            {
                int typeIndex = rnd.Next(0, 3);
                int reloadTime=rnd.Next(0, 5);
                if (item.Key.Length > item.Value)
                    w.Add(new Weapon((Enum.Type)typeIndex, item.Key, item.Value, item.Key.Length,reloadTime));
                else
                    w.Add(new Weapon((Enum.Type)typeIndex, item.Key, item.Key.Length, item.Value, reloadTime));                        

                   
            }

            Armory a = Armory.Instance;

            foreach(var weapon in w)
            {
                if (!a.Weapons.Contains(weapon)){
                    a.Weapons.Add(weapon);
                }
            }
        }
    }
}
