﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class B_Wings : SpaceShip
    {
        public B_Wings(): base(30,0)
        {
            AddWeapon(new Weapon(Enum.Type.Explosive, "Hammer", 1, 8, 1.5));
        }

        public override void AddWeapon(Weapon w)
        {
            if (w.Type == Enum.Type.Explosive)
                w.ReloadTime = 1;
            
            base.AddWeapon(w);
        }

        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("B_Wings attaque le joueur :");
            base.Attack(spaceShip);
        }
    }
}
