﻿using System;
using Type = fanny_eber_TP1.Enum.Type;

namespace fanny_eber_TP1.Class
{
    public class Weapon
    {
        private string Name { get; }
        public int MinDamage { get; }
        public int MaxDamage { get; }
        private Type Type { get;  }

        public Weapon(Type type, string name, int minDamage, int maxDamage)
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            Type = type;
        }

        //Moyenne des dégat de l'arme
        public double AverageDamage() => (MaxDamage + MinDamage) / 2.0;

        //Affiche les caractéristiques 
        public override string ToString()
        {
            return $"{Name}\nDégats maximum : {MaxDamage}\nDégats minimum : {MinDamage}\nType d'arme : {Type}\n";
        }

        //Compare deu armes entre elles pour vérifier si ce sont les mêmes
        public override bool Equals(object? obj)
        {
            return (obj is Weapon other) && Name == other.Name && MinDamage == other.MinDamage && MaxDamage == other.MaxDamage && Type == other.Type;
        }
       
    }
}