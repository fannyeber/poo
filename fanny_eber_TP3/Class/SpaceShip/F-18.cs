﻿using fanny_eber_TP1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class F_18 : SpaceShip, IAptitude
    {
        public F_18() : base(15, 0)
        {

        }

        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("F_18 n'a pas d'arme, il ne peut pas attaquer \n");
        }

        public void Use(List<SpaceShip> spaceShips)
        {
            bool hasBlewnUp = false;
            //si F_18 a coté de ViperMKII => explose et du coup il meurt
            //fait 10 points de dégats à tout les vaisseaux autour 
            for(int i=0;i<spaceShips.Count && !hasBlewnUp;i++)
            {
                if (spaceShips[i] == this)
                {
                    if (i == 0)
                    {
                        if (spaceShips[i + 1].GetType() == typeof(ViperMKII))
                        {
                            spaceShips[i - 1].TakeDamage(10);
                            spaceShips[i + 1].TakeDamage(10);
                            spaceShips[i].StructurePoints = 0;
                            spaceShips[i].ShieldPoints = 0;
                            hasBlewnUp = true;
                            Console.WriteLine($"F_18 utilise attaque spéciale : EXPLOSION !\n");
                        }

                    }
                    else if (i == spaceShips.Count - 1)
                    {
                        if (spaceShips[i - 1].GetType() == typeof(ViperMKII))
                        {
                            spaceShips[i - 1].TakeDamage(10);
                            spaceShips[i].StructurePoints = 0;
                            spaceShips[i].ShieldPoints = 0;
                            hasBlewnUp = true;
                            Console.WriteLine($"F_18 utilise attaque spéciale : EXPLOSION !");
                        }
                    }
                    else
                    {
                        if(spaceShips[i - 1].GetType() == typeof(ViperMKII) || spaceShips[i + 1].GetType() == typeof(ViperMKII))
                        {
                            spaceShips[i - 1].TakeDamage(10);
                            spaceShips[i + 1].TakeDamage(10);
                            spaceShips[i].StructurePoints = 0;
                            spaceShips[i].ShieldPoints = 0;
                            hasBlewnUp = true;
                            Console.WriteLine($"F_18 utilise attaque spéciale : EXPLOSION !");
                        }
                    }
                }
                    
            }
        }
    }
}
