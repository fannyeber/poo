﻿using fanny_eber_TP1;
using fanny_eber_TP1.Class;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace fanny_eber_TP4.ViewModel
{
    public class MenuViewModel : INotifyPropertyChanged
    {
        
        public event PropertyChangedEventHandler PropertyChanged;

        private Armory _armory=Armory.Instance;

        private ObservableCollection<Weapon> _weaponList;
        public ObservableCollection<Weapon> WeaponList
        {
            get => _weaponList;
            set
            {
                _weaponList = value;
                OnPropertyChanged("WeaponList");
            }
        }

        private WeaponImportor importor = new WeaponImportor();
        private SpaceInvaders game = new SpaceInvaders();

        public MenuViewModel()
        {
            _weaponList = new ObservableCollection<Weapon>(_armory.Weapons);
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName= null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        

        public void BrowseFile(TextBox textBox, DataGrid lst)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            
            bool? result = openFileDlg.ShowDialog();

            if (result==true)
            {
                textBox.Text = openFileDlg.FileName;
                importor.generateWeapon(openFileDlg.FileName);
                
                WeaponList = new ObservableCollection<Weapon>(_armory.Weapons);
            }
        }

        public void DisplayPlayer(ListBox lstPlayers)
        {
            lstPlayers.Items.Clear();
            foreach (var p in game.Players)
            {
                lstPlayers.Items.Add(p.ToString());
            }
        }

        public void BrowseImage(Image imgShip, int indexJoueur)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();

            Nullable<bool> result = openFileDlg.ShowDialog();

            if (result == true)
            {
                game.Players[indexJoueur].Ship.image.Source= new BitmapImage(new Uri(openFileDlg.FileName));
                imgShip.Source = game.Players[indexJoueur].Ship.image.Source;
            }
            
        }

        public void RemovePlayer(int indexJoueur, ListBox lst)
        {
            game.Players.RemoveAt(indexJoueur);
            DisplayPlayer(lst);
        }

        public void AddPlayer(string lastName, string firstName, string pseudo,ListBox lst)
        {
            game.Players.Add(new Player(firstName,lastName,pseudo));
            DisplayPlayer(lst);
        }

        public ImageSource loadImage(int index)
        {
            if (index >= 0)
            {
                
                return game.Players[index].Ship.image.Source ?? new BitmapImage(new Uri("../View/missingImage.png", UriKind.Relative));
            }
            
            return new BitmapImage(new Uri("../View/missingImage.png", UriKind.Relative));

        }

        public bool checkInformations(string text)
        {
            return !string.IsNullOrEmpty(text);
        }

        public void DisplayShipWeapon(DataGrid dtgShipWeapons, int index)
        {
            dtgShipWeapons.ItemsSource = game.Players[index]?.Ship?.Weapons;
        }

        public void DisplayWeapon(DataGrid dtgArmory)
        {
            dtgArmory.ItemsSource = _armory.Weapons;
        }

        public void DeleteShipWeapon(int indexPlayer, int indexWeapon)
        {
            if (indexPlayer != -1 && indexWeapon != -1 && indexPlayer<game.Players.Count )
            {
                var p = game.Players[indexPlayer];
                if (indexWeapon < p.Ship.Weapons.Count)
                {
                    var w = p.Ship.Weapons[indexWeapon];
                    p.Ship.RemoveWeapon(w);
                }

            }
        }

        public void AddShipWeapon(int indexPlayer, int indexWeapon)
        {
            if (indexPlayer > -1 && indexPlayer < game.Players.Count &&
                indexWeapon > -1 && indexWeapon < _armory.Weapons.Count)
            {
                game.Players[indexPlayer].Ship.AddWeapon(_armory.Weapons[indexWeapon]);
            }
        }


    }
}
