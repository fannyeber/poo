﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class ViperMKII : SpaceShip
    {
        public ViperMKII() : base(10, 15)
        {
            AddWeapon(new Weapon(Enum.Type.Direct, "Mitrailleuse", 2, 3, 1));
            AddWeapon(new Weapon(Enum.Type.Explosive, "EMG", 1, 7, 1.5));
            AddWeapon(new Weapon(Enum.Type.Guided, "Missile", 4, 100, 4));
        }

        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("Le joueur attaque un vaisseau ennemi : ");
            Weapons.ForEach(w => w.CountRound--);

            var rnd = new Random();
            var start = rnd.Next(0, Weapons.Count);
            bool hasShoot = false;

            for (int i = 0; i < Weapons.Count && !hasShoot; i++)
            {
                if (Weapons[start].CountRound == 0)
                {
                    int damage = Weapons[start].Shot();
                    spaceShip.TakeDamage(damage);

                    if (spaceShip.GetIsDestroy())
                        Console.WriteLine($"Dégats: {damage}\nDestruction : OUI \n");
                    else
                        Console.WriteLine($"Dégats: {damage}\nDestruction : NON \n");
                }

                start++;
                if (start == Weapons.Count)
                    start = 0;
            }
        }
    }
}
