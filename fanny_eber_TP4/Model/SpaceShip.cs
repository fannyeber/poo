﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public abstract class SpaceShip
    {
        private bool IsDestroy;

        public int MaxStructurePoints { get; }
        public int MaxShieldPoints { get; }

        public int StructurePoints { get; set; }
        public int ShieldPoints { get; set; }

        public ObservableCollection<Weapon> Weapons { get; }

        public Image image { get; set; }

        public SpaceShip(int structure, int shield)
        {
            Weapons = new ObservableCollection<Weapon>();
            MaxStructurePoints = StructurePoints = structure;
            MaxShieldPoints = ShieldPoints = shield;
            IsDestroy = false;
            image = new Image();
        }

        //Permet d'ajouter une arme du vaisseau mais il faut que cette arme soit dans l'armurerie et qu'il y en ai moins de 3 dans le vaisseau
        public virtual void AddWeapon(Weapon w){
            Armory armory = Armory.Instance;
            if (armory.Weapons.Contains(w) && Weapons.Count < 3)
            {
                Weapons.Add(w);
            }
        }


        //Permet de retirer une arme du vaisseau
        public void RemoveWeapon(Weapon w)=>Weapons.Remove(w);


        //Permet de savoir si le vaisseau est détruit ou non 
        public bool GetIsDestroy()
        {
            if (StructurePoints == 0 && ShieldPoints == 0)
                return IsDestroy = true;
            return IsDestroy=false;
        }

        //permet de prendre des dégat 
        public void TakeDamage(int damage)
        {
            if (ShieldPoints-damage >=0)
            {
                ShieldPoints -= damage;
            }
            else if (ShieldPoints > 0)
            {
                damage -= ShieldPoints;
                ShieldPoints = 0;
                StructurePoints -= damage;
            }
            else
            {
                StructurePoints = StructurePoints >= damage ? StructurePoints - damage : 0;
            }
        }

        //permet de connaitre la moyenne des dégat que le vaisseau peut infliger
        public double TotalDamage()
        {
            double damage=0;
            int nbWeapons = 0;
            foreach (var w in Weapons)
            {
                damage += w.AverageDamage();
                nbWeapons++;
            }
            return damage/nbWeapons;
        }


        //Affiche si les caractéristiques du vaisseau
        public override string ToString()
        {
            string text = "";
            if (IsDestroy)
                text += "Le vaisseau est détruit \n";
            else
                text += "Le vaisseau n'est pas détruit\n";
            //text += $"Maximum de points de structure : {MaxStructurePoints} \n";
            //text += $"Maximum de points de bouclier : {MaxShieldPoints}\n";
            text += $"Point de structure : {StructurePoints}\n";
            text += $"Point du boulier : {ShieldPoints}\n";
            /*text += "Liste d'armes : \n";
            foreach (var w in Weapons)
            {
                text += w.ToString();
            }*/
            return text;
        }


        public virtual void Attack(SpaceShip spaceShip)
        {
            foreach (var w in Weapons)
            {
                w.CountRound--;
            }

            var rnd = new Random();
            var start = rnd.Next(0, Weapons.Count);

            int damage = Weapons[start].Shot();
            spaceShip.TakeDamage(damage);

            if (spaceShip.GetIsDestroy())
                Console.WriteLine($"Dégats: {damage}\nDestruction : OUI \n");
            else
                Console.WriteLine($"Dégats: {damage}\nDestruction : NON \n");
              
        }
    }


}