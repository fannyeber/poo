﻿namespace fanny_eber_TP1.Enum
{
    //Différents type d'arme
    public enum Type
    {
        Direct,
        Explosive,
        Guided,
    }
}