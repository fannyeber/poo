﻿using System;
using System.Collections.Generic;
using fanny_eber_TP1.Class;

namespace fanny_eber_TP1
{
    class SpaceInvaders
    {
        private List<Player> Players { get; }
        private Armory Armory { get; }

        public SpaceInvaders()
        {
            Players=Init();
            Armory = Armory.Instance;
        }

        //Initialise la liste de joueur
        private List<Player> Init()
        {
            List<Player> players = new();
            players.Add(new Player("jEaN","NeYmar","kikooLol48"));
            players.Add(new Player("JeAn", "Bon", "bg_du_67"));
            players.Add(new Player("Emma", "KarEna", "choupetteDuLove"));

            return players;
        }
        
        //Méthode appelé en premier a l'execution
        static void Main()
        {
            SpaceInvaders game = new();

            Console.WriteLine("JOUEURS : \n");
            foreach (var p in game.Players)
            {
                Console.WriteLine(p.ToString());
                Console.WriteLine(p.Ship.ToString());
                Console.WriteLine("----------------------------");
            }

            Console.WriteLine("ARMURERIE : ");
            Console.WriteLine(game.Armory.ToString());
            Console.WriteLine("----------------------------");
        }


    }
}
