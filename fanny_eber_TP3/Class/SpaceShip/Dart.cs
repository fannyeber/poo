﻿using System;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class Dart : SpaceShip
    {
        public Dart(): base(10,3)
        {
            AddWeapon(new Weapon(Enum.Type.Direct, "Laser", 2, 3, 1));
        }

        public override void AddWeapon(Weapon w)
        {
            if (w.Type == Enum.Type.Direct)
                w.ReloadTime = 1;
            base.AddWeapon(w);
        }

        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("Dart attaque le joueur :");
            base.Attack(spaceShip);
        }
    }
}
