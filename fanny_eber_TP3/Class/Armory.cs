﻿using System;
using System.Collections.Generic;
using System.Linq;
using Type = fanny_eber_TP1.Enum.Type;

namespace fanny_eber_TP1.Class
{
    public class Armory
    {
        #region Patern singleton
        private static readonly Lazy<Armory> lazy = new Lazy<Armory>(() => new Armory());

        public List<Weapon> Weapons { get; set; }

        public static Armory Instance
        {
            get { return lazy.Value; }
        }
        private Armory()
        {
            Init();
        }

        #endregion Patern singleton
        //Initialise les armes de l'armurerie
        private void Init()
        {
            Weapons = new List<Weapon>();
            Weapons.Add(new Weapon(Enum.Type.Direct, "Laser", 2, 3, 1));
            Weapons.Add(new Weapon(Enum.Type.Explosive, "Hammer", 1, 8, 1.5));
            Weapons.Add(new Weapon(Enum.Type.Guided, "Torpille", 3, 3, 2));
            Weapons.Add(new Weapon(Enum.Type.Direct, "Mitrailleuse", 2, 3, 1));
            Weapons.Add(new Weapon(Enum.Type.Explosive, "EMG", 1, 7, 1.5));
            Weapons.Add(new Weapon(Enum.Type.Guided, "Missile", 4, 100, 4));
        }

        public List<Weapon> Get5BiggestAverageDamageWeapons()
        {
            return Weapons.OrderBy(x => x.AverageDamage()).Take(5).ToList();
        }
        public List<Weapon> Get5BiggestMinDamageWeapons()
        {
            return Weapons.OrderBy(x => x.MinDamage).Take(5).ToList();
        }

        //Affiche a liste d'arme de l'armurerie 
        public override string ToString()
        {
            string text = "";
            foreach (var w in Weapons)
            {
                text+=w.ToString();
                text += "\n";
            }
            

            return text;
        }
    }
}