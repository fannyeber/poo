﻿using System;
using System.Collections.Generic;

namespace fanny_eber_TP1.Class
{
    public class SpaceShip
    {
        private bool IsDestroy;

        private int MaxStructurePoints { get; }
        private int MaxShieldPoints { get; }

        private int StructurePoints { get; set; }
        private int ShieldPoints { get; set; }

        private List<Weapon> Weapons { get; }

        public SpaceShip()
        {
            Weapons = new List<Weapon>(3);
            MaxShieldPoints = StructurePoints = 100;
            MaxStructurePoints = ShieldPoints = 100;
            IsDestroy = false;
        }

        //Permet d'ajouter une arme du vaisseau mais il faut que cette arme soit dans l'armurerie et qu'il y en ai moins de 3 dans le vaisseau
        public void AddWeapon(Weapon w){
            Armory armory = Armory.Instance;
            if (!armory.Weapons.Contains(w))
                throw new ArmoryException("Cette arme n'existe pas dans l'armurerie");
            if (Weapons.Count >= 3)
                throw new Exception("Votre stock d'arme est déjà plein");
            Weapons.Add(w);
        }

        //Permet de retirer une arme du vaisseau
        public void RemoveWeapon(Weapon w)=>Weapons.Remove(w);


        //Permet de savoir si le vaisseau est détruit ou non 
        public bool GetIsDestroy()
        {
            if (StructurePoints == 0 && ShieldPoints == 0)
                return IsDestroy = true;
            return IsDestroy=false;
        }

        //permet de prendre des dégat 
        public void TakeDamage(int damage)
        {
            if (ShieldPoints >=damage)
            {
                ShieldPoints -= damage;
            }
            else if (ShieldPoints > 0)
            {
                damage -= ShieldPoints;
                ShieldPoints = 0;
                StructurePoints -= damage;
            }
            else
            {
                StructurePoints = StructurePoints >= damage ? StructurePoints - damage : 0;
            }
        }

        //permet de connaitre la moyenne des dégat que le vaisseau peut infliger
        public double TotalDamage()
        {
            double damage=0;
            int nbWeapons = 0;
            foreach (var w in Weapons)
            {
                damage += w.AverageDamage();
                nbWeapons++;
            }
            return damage/nbWeapons;
        }

        //Affiche si les caractéristiques du vaisseau
        public override string ToString()
        {
            string text = "";
            if (IsDestroy)
                text += "Le vaisseau est détruit \n";
            else
                text += "Le vaisseau n'est pas détruit\n";
            text += $"Maximum de points de structure : {MaxStructurePoints} \n";
            text += $"Maximum de points de bouclier : {MaxShieldPoints}\n";
            text += $"Point de structure : {StructurePoints}\n";
            text += $"Point du boulier : {ShieldPoints}\n";
            text += "Liste d'armes : \n";
            foreach (var w in Weapons)
            {
                text += w.ToString();
            }
            return text;
        }
    }
}