﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using fanny_eber_TP1;

namespace fanny_eber_TP4.View
{
    public partial class AddWeaponView : Window
    {
        private Window menu;
        private int indexPlayer;
        public AddWeaponView(Window menuWindow,int idPlayer)
        {
            menu = menuWindow;
            indexPlayer = idPlayer;
            MenuView v = ((MenuView)menu);
            InitializeComponent();
            v.viewModel.DisplayWeapon(dtgArmory);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            MenuView v=((MenuView) menu);
            if (dtgArmory.SelectedIndex!=-1)
            {
                v.viewModel.AddShipWeapon(indexPlayer, dtgArmory.SelectedIndex);
                this.Close();
            }

        }
    }
}
