﻿namespace fanny_eber_TP1.Class
{
    public class Player
    {
        private string Name { get; }
        public string Pseudo { get; set; }
        public SpaceShip Ship { get; }

        public Player(string firstName, string lastName, string pseudo)
        {
            Ship = new SpaceShip();
            Name = $"{StringFormat(firstName)} {StringFormat(lastName)}";
            Pseudo = pseudo;
        }

        //renvoie une chaine de caractère avec la premiere en majuscule et la suite en minuscule
        private static string StringFormat(string name)
        {
            return name.Substring(0, 1).ToUpper() + name.Substring(1).ToLower();
        }

        //Affiche le pseudo du joueur et son nom, prénom
        public override string ToString()=> $"{Pseudo} ({Name})";

        //Permet de comparer un joueur a un autre joueur par rapport a leur pseudo
        public override bool Equals(object? obj)=>(obj is Player p) && Pseudo == p.Pseudo;

    }
}
