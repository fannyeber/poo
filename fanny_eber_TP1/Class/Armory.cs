﻿using System;
using System.Collections.Generic;
using Type = fanny_eber_TP1.Enum.Type;

namespace fanny_eber_TP1.Class
{
    public class Armory
    {
        #region Patern singleton
        private static readonly Lazy<Armory> lazy = new Lazy<Armory>(() => new Armory());

        public List<Weapon> Weapons { get; set; }

        public static Armory Instance
        {
            get { return lazy.Value; }
        }
        private Armory()
        {
            Init();
        }

        #endregion Patern singleton
        //Initialise les armes de l'armurerie
        private void Init()
        {
            Weapons = new List<Weapon>();
            Weapons.Add(new Weapon(Type.Direct,"sniper",10,100));
            Weapons.Add(new Weapon(Type.Explosive,"grenade",5,70));
            Weapons.Add(new Weapon(Type.Guided,"tête chercheuse", 20, 80));
        }

        //Affiche a liste d'arme de l'armurerie 
        public override string ToString()
        {
            string text = "";
            foreach (var w in Weapons)
            {
                text+=w.ToString();
            }

            return text;
        }
    }
}