﻿

using System;
using System.Linq;
using System.Threading;

namespace fanny_eber_TP0
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables
            string lastName = string.Empty;
            string firstName = string.Empty;
            int age = 0;
            int size = 0;
            float weight = 0;
            float imc = 0;
            int nbHair = 0;
            int choice=0;
            bool stop=false;

            while(!stop){
                //Interaction avec utilisateur
                Console.WriteLine("Bienvenue sur mon programme, jeune étranger imberbe");

                //nom et prénom
                Console.WriteLine("Donne moi ton nom, vil chenapan : ");
                lastName = CheckString("Il n'y a pas de chiffre dans ton nom, entre ton vrai nom :");

                Console.WriteLine("Et quel est ton prénom, petit galopin : ");
                firstName = CheckString("Il n'y a pas de chiffre dans ton prénom, entre ton vrai prénom :");

                Console.WriteLine($"Bonjour {CreateName(lastName,firstName)} !");

                //taille poids et age
                Console.WriteLine("Quel est ta taille (en cm) : ");
                size=CheckInt("Votre taille est forcément un nombre supérieur à 0:");
                
                Console.WriteLine("Quel age as-tu :");
                age = CheckInt("Votre age est forcément un nombre supérieur à 0:");
                
                Console.WriteLine("Quel est ton poids (en kg) :");
                while (!float.TryParse(Console.ReadLine(), out weight) || weight<0)
                {
                    Console.WriteLine("Votre poids est forcément un nombre supérieur à 0:");
                }

                if (age < 18)
                {
                    Console.WriteLine("AhAh ! Tu es un morveux puéril et immature qui n’a même pas le droit d’acheter de l’alcool en grande surface");
                }

                imc = CreateIMC(weight, size);
                Console.WriteLine($"Votre IMC est de {imc.ToString()}");

                CommentIMC(imc);

                //cheveux
                Console.WriteLine("Estimez le nombre de cheveux qu'il y a sur votre tête : ");
                while(!int.TryParse(Console.ReadLine(), out nbHair) || !(nbHair>=100000 && nbHair <= 150000))
                {
                    Console.WriteLine("Vous avez forcément entre 100 000 et 150 000 cheveux sur votre tête, essayez encore :");
                }

                //fin du programme
                Console.WriteLine("Vous avez le choix entre 4 actions : \n"+
                                    "1 : Quitter le programme \n"+
                                    "2 : Recommencer le programme \n"+
                                    "3 : Compter jusqu'à 10 \n"+
                                    "4 : Téléphoner à Tata Jacqueline \n");
                while(!int.TryParse(Console.ReadLine(), out choice) && choice<=0 && choice>4){
                    Console.WriteLine("Vous devez chosir soit 1, 2, 3 ou 4 : ");
                }

                switch(choice){
                    case 1:
                        QuitApplication();
                        stop=true;
                        break;
                    case 2:
                        Console.WriteLine("C'est reparti pour un tour ! \n");
                        break;
                    case 3:
                        CountToTen();
                        stop=true;
                        break;
                    case 4:
                        CallJacqueline();
                        stop=true;
                        break;
                    default:
                        Console.WriteLine($"Error le nombre {choice} n'est pas une possibilité");
                        break;
                }
            }

            return;

        }

        static string CreateName(string lastName, string firstName) => $"{firstName.ToLower()} {lastName.ToUpper()}";

        static float CreateIMC(float weight, int size) => (float) (weight / Math.Pow(size / 100.0, 2));

        static void CommentIMC(float imc)
        {
            const string BIG_UNDER_WEIGHT="Attention à l’anorexie!";
            const string UNDER_WEIGHT = "Vous êtes un peu maigrichon!";
            const string NORMAL_WEIGHT = "Vous êtes de corpulence normale !";
            const string OVER_WEIGHT = "Vous êtes en surpoids !";
            const string BIG_OVER_WEIGHT = "Obésité modérée !";
            const string BIG_BIG_OVER_WEIGHT = "Obésité sévère !";
            const string DEAD_OVER_WEIGHT = "Obésité morbide !";

            if (imc < 16.5)
                Console.WriteLine(BIG_UNDER_WEIGHT);
            else if (imc >= 16.5 && imc < 18.5)
                Console.WriteLine(UNDER_WEIGHT);
            else if (imc >= 18.5 && imc < 25)
                Console.WriteLine(NORMAL_WEIGHT);
            else if (imc >= 25 && imc < 30)
                Console.WriteLine(OVER_WEIGHT);
            else if (imc >= 30 && imc < 35)
                Console.WriteLine(BIG_OVER_WEIGHT);
            else if (imc >= 35 && imc < 40)
                Console.WriteLine(BIG_BIG_OVER_WEIGHT);
            else
                Console.WriteLine(DEAD_OVER_WEIGHT);
        }

        static int CheckInt(string errorSentence){
            int variable=0;
            while (!int.TryParse(Console.ReadLine(), out variable) || variable < 0)
                Console.WriteLine(errorSentence);
            
            return variable;
        }

        static string CheckString(string errorSentence){
            string variable=string.Empty;
            variable=Console.ReadLine();
            while(variable.Any(char.IsDigit)){
                Console.WriteLine(errorSentence);
                variable=Console.ReadLine();
            }

            return variable;
        }

        static void QuitApplication(){
            Console.WriteLine("Au plaisir de vous revoir cher ami");
            Thread.Sleep(3000);
        }

        static void CountToTen(){

            for(int i=0;i<10;i++){
                Console.WriteLine((i+1).ToString());
                Thread.Sleep(1000);
            }
            QuitApplication();
        }

        static void CallJacqueline(){
            Console.WriteLine("Bonjour ! Je suis pas là pour l'instant, ou alors je ne veux pas te répondre\n"+
            "Ne me rappelez plus jamais \n"+
            "Bisous, Bonne journée");
            QuitApplication();
        }

    }
}
