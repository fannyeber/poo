﻿using System;
using System.Collections.ObjectModel;
using Type = fanny_eber_TP1.Enum.Type;

namespace fanny_eber_TP1.Class
{
    public class Weapon  
    {
        public string Name { get; }
        public int MinDamage { get; }
        public int MaxDamage { get; }
        public Type Type { get;  }
        public double ReloadTime { get; set; }
        public double CountRound { get; set; }

        public Weapon(Type type, string name, int minDamage, int maxDamage, double reloadTime)
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            Type = type;
            ReloadTime = reloadTime;
            CountRound = Type==Type.Explosive ? reloadTime*2 : reloadTime;
        }


        /**
         * Simulation de tir si l'arme est rechargé
         */
        public int Shot()
        {

            if (CountRound <= 0)
            {
                var rnd = new Random();
                var damage = rnd.Next(MinDamage, MaxDamage);
                switch (Type)
                {
                    case Type.Explosive:
                        CountRound = ReloadTime * 2;
                       return rnd.Next(0, 4)==0 ? 0 : damage;
                    case Type.Direct:
                        CountRound = ReloadTime;
                        return rnd.Next(0, 10) == 0 ? 0 : damage;
                    case Type.Guided:
                        return MinDamage;
                    default:
                        return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        //Moyenne des dégat de l'arme
        public double AverageDamage() => (MaxDamage + MinDamage) / 2.0;

        //Affiche les caractéristiques 
        public override string ToString()
        {
            return $"{Name}\nDégats maximum : {MaxDamage}\nDégats minimum : {MinDamage}\nType d'arme : {Type}\n";
        }

        //Compare deu armes entre elles pour vérifier si ce sont les mêmes
        public override bool Equals(object obj)
        {
            return (obj is Weapon other) && Name == other.Name && MinDamage == other.MinDamage && MaxDamage == other.MaxDamage && Type == other.Type;
        }
       
    }
}