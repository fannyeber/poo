﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using fanny_eber_TP1;

namespace fanny_eber_TP4.View
{
    /// <summary>
    /// Logique d'interaction pour AddPlayerView.xaml
    /// </summary>
    public partial class AddPlayerView : Window
    {
        private Window menu;
        public AddPlayerView(Window menuWindow)
        {
            menu = menuWindow;
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            MenuView v=((MenuView) menu);
            if (v.viewModel.checkInformations(txtLastName.Text) &&
                v.viewModel.checkInformations(txtFirstName.Text) &&
                v.viewModel.checkInformations(txtPseudo.Text))
            {
                v.viewModel.AddPlayer(txtLastName.Text, txtFirstName.Text, txtPseudo.Text, v.lstPlayers);
                this.Close();
            }

        }
    }
}
