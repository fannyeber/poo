﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class Rocinante : SpaceShip
    {
        public Rocinante() : base(3,5)
        {
            AddWeapon(new(Enum.Type.Guided, "Torpille", 3, 3, 2));
        }
        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("Rocinante attaque le joueur :");
            base.Attack(spaceShip);
        }
    }
}
