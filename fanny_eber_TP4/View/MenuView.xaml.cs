﻿using fanny_eber_TP4.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fanny_eber_TP4.View
{
    /// <summary>
    /// Logique d'interaction pour MenuView.xaml
    /// </summary>
    public partial class MenuView : Window
    {
        public MenuViewModel viewModel=new MenuViewModel();
        private Window addPlayer;
        private Window addWeapon;

        public MenuView()
        {
            InitializeComponent();

            grpPlayer.Visibility = Visibility.Hidden;
            viewModel.DisplayPlayer(lstPlayers);
            viewModel.DisplayWeapon(dtgWeapons);
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            viewModel.BrowseFile(txtFile,dtgWeapons);
        }

        private void lstPlayers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lst = (ListBox) sender;

            if (lst.SelectedIndex != -1)
            {
                grpPlayer.Header = lst.SelectedItem;
                imgShip.Source = viewModel.loadImage(lst.SelectedIndex);
                viewModel.DisplayShipWeapon(dtgShipWeapons, lst.SelectedIndex);
                grpPlayer.Visibility = Visibility.Visible;
            }
            else
            {
                imgShip.Source = viewModel.loadImage(lst.SelectedIndex);

                grpPlayer.Visibility = Visibility.Hidden;
            }

        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            viewModel.BrowseImage(imgShip,lstPlayers.SelectedIndex );
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            viewModel.RemovePlayer(lstPlayers.SelectedIndex,lstPlayers);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            addPlayer=new AddPlayerView(this);
            addPlayer.Show();
        }

        private void btnDeleteWeapon_Click(object sender, RoutedEventArgs e)
        {
            viewModel.DeleteShipWeapon(lstPlayers.SelectedIndex, dtgShipWeapons.SelectedIndex);
        }

        private void btnAddWeapon_Click(object sender, RoutedEventArgs e)
        {
            addWeapon = new AddWeaponView(this, lstPlayers.SelectedIndex);
            addWeapon.Show();
        }
    }
}
