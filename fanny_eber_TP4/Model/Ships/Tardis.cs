﻿using fanny_eber_TP1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fanny_eber_TP1.Class.SpaceShip
{
    public class Tardis : SpaceShip, IAptitude
    {
        public Tardis() : base(1, 0)
        {

        }
        public override void Attack(SpaceShip spaceShip)
        {
            Console.WriteLine("Tardis n'a pas d'arme, il ne peut pas attaquer\n");

        }

        public void Use(List<SpaceShip> spaceShips)
        {
            var rnd = new Random();
            var i = rnd.Next(0, spaceShips.Count);
            var j = rnd.Next(0, spaceShips.Count);
            var ship = spaceShips[i];
            spaceShips[i] = spaceShips[j];
            spaceShips[j] = ship;
            Console.WriteLine("Tardis utilise attaque spéciale : Change les vaisseaux de place !\n");
        }
    }
}
